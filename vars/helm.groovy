/**
 * Copyright 2018 The Cloudflavor authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 *
 * This groovy library is meant as a wrapper for kubernetes Helm operations
 *
 *
 * @certificatePath path to the kubernetes certificates
 * @chartsPath path to the Helm charts that we want to use to create a new deployment
 * @helmBinPath path to the Helm binary
 */


import com.cloudflavor.io.Helm;

def call(Map parameters = [:]) {
    def k8sCertificatePath = parameters.get('certificatePath')
    def chartsPath = parameters.get('chartsPath')
    def helmBinPath = parameters.get('helmBinPath')

    newHelm = helm.Helm(
        k8sCertificatePath,
        helmBinPath,
    )
    echo "${newHelm.binPath}"
}
